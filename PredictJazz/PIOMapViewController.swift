
//
//  ViewController.swift
//  PredictJazz
//
//  Created by Cole GOODIER on 6/26/18.
//  Copyright © 2018 Cole Goodier Jasint. All rights reserved.
//

import UIKit
import CoreData
import Foundation
import MapKit
import PredictIO

class PIOMapViewController: UIViewController, MKMapViewDelegate {
    
    var location: CLLocation!
    var zoneType = PIOZoneType.other
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var zoneLabel: UILabel!
    @IBOutlet weak var zoneView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Location"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let coordinate = self.location.coordinate
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = "Event location" //You can set the subtitle too
        mapView.addAnnotation(annotation)
        
        mapView.add(MKCircle(center: coordinate, radius: self.location.horizontalAccuracy))
        
        let coordinateDelta:CLLocationDegrees = 0.003
        let span = MKCoordinateSpanMake(coordinateDelta, coordinateDelta)
        let region = MKCoordinateRegionMake(coordinate, span)
        mapView.setRegion(region, animated: true)
        
        // display zone information if available
        switch zoneType {
        case .home:
            zoneLabel.text = "Event triggered within Home zone"
            zoneView.isHidden = false
            break
        case .work:
            zoneLabel.text = "Event triggered within Work zone"
            zoneView.isHidden = false
            break
        case .other:
            zoneView.isHidden = true
            break
        }
    }
    
    
}
