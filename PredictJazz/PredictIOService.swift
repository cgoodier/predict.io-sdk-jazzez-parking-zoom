
//
//  ViewController.swift
//  PredictJazz
//
//  Created by Cole GOODIER on 6/26/18.
//  Copyright © 2018 Cole Goodier Jasint. All rights reserved.
//
import UIKit
import Foundation
import CoreData
import PredictIO

enum PredictIOEventType: Int {
    case departing = 0
    case departed
    case departureCanceled
    case arrivalSuspected
    case arrived
    case stationary
     case searching
}

class PredictIOService: NSObject, PredictIODelegate {

    let apiKey = "71a3ba3d99ded564840cdc41f746ac70bc06a4f9ff19dac9bacd6d142b762674"

    override init() {
        super.init()

        let predictIO = PredictIO.sharedInstance()
        predictIO?.delegate = self
        predictIO?.apiKey = apiKey

        NotificationCenter.default.addObserver(self, selector:#selector(departingViaNotification), name:NSNotification.Name.PIODeparting, object:nil)
        NotificationCenter.default.addObserver(self, selector:#selector(departedViaNotification), name:NSNotification.Name.PIODeparted, object:nil)
        NotificationCenter.default.addObserver(self, selector:#selector(departureCanceledViaNotification), name:NSNotification.Name.PIOCanceledDeparture, object:nil)
        NotificationCenter.default.addObserver(self, selector:#selector(arrivalSuspectedViaNotification), name:NSNotification.Name.PIOSuspectedArrival, object:nil)
        NotificationCenter.default.addObserver(self, selector:#selector(arrivedViaNotification), name:NSNotification.Name.PIOArrived, object:nil)
        NotificationCenter.default.addObserver(self, selector:#selector(stationaryAfterArrivalViaNotification), name:NSNotification.Name.PIOBeingStationaryAfterArrival, object:nil)
         NotificationCenter.default.addObserver(self, selector:#selector(searchingInPerimeterViaNotification), name:NSNotification.Name.PIOSearchingParking, object:nil)
    
    }

    func resume() -> Void {
        // Check if PredictIO was set to run on every app launch
        let defaults = UserDefaults.standard
        let predictIOEnabled = defaults.bool(forKey: "PredictIOEnabled")
        if predictIOEnabled {
            PredictIO.sharedInstance().start(completionHandler: { (error) -> (Void) in
                if let error = error {
                    OperationQueue.main.addOperation({
                        let userInfo = error._userInfo as! NSDictionary
                        let errorTitle = userInfo["NSLocalizedFailureReason"] as! String
                        let errorDescription = userInfo["NSLocalizedDescription"] as! String
                        let rootViewController = UIApplication.shared.keyWindow?.rootViewController;
                        let alertController = UIAlertController(title: errorTitle, message: errorDescription, preferredStyle: .alert)
                        let alertActionOK = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertController.addAction(alertActionOK)
                        rootViewController!.present(alertController, animated: true, completion: nil)
                        print("<predict.io> API Key: \(errorTitle) \(errorDescription)")
                        defaults.set(false, forKey: "PredictIOEnabled")
                        defaults.synchronize()
                    })
                } else {
                    print("Started predict.io... \(PredictIO.sharedInstance().deviceIdentifier() ?? "")")
                }
            })
        }
    }

    func startWithCompletionHandler(_ handler: @escaping (_ error: Error?) -> (Void) ) -> Void {
        PredictIO.sharedInstance().start(completionHandler: handler)
        // set to run PredictIO on every app launch
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "PredictIOEnabled")
        defaults.synchronize()
    }

    func stop() -> Void {
        PredictIO.sharedInstance().stop()
        // unset to run PredictIO on next app launch
        let defaults = UserDefaults.standard
        defaults.set(false, forKey: "PredictIOEnabled")
        defaults.synchronize()
    }

    // Mark - PredictIO Delegate methods

    func departing(_ tripSegment: PIOTripSegment!) {
        self.insertEventViaDelegate(.departing, location: tripSegment.departureLocation, transportationMode: tripSegment.transportationMode, stationary: tripSegment.stationaryAfterArrival, zoneType: tripSegment.departureZone.zoneType);
        print("Delegate - departing")
    }

    func departed(_ tripSegment: PIOTripSegment) {
        self.insertEventViaDelegate(.departed, location: tripSegment.departureLocation, transportationMode: tripSegment.transportationMode, stationary: tripSegment.stationaryAfterArrival, zoneType: tripSegment.departureZone.zoneType);
        print("Delegate - departed")
    }

    func departureCanceled(_ tripSegment: PIOTripSegment) {
        self.insertEventViaDelegate(.departureCanceled, location: tripSegment.departureLocation, transportationMode: tripSegment.transportationMode, stationary: tripSegment.stationaryAfterArrival, zoneType: tripSegment.departureZone.zoneType);
        print("Delegate - departureCanceled")
        
        func arrivalSuspected(_ tripSegment: PIOTripSegment) {
            self.insertEventViaDelegate(.arrivalSuspected, location: tripSegment.arrivalLocation, transportationMode: tripSegment.transportationMode, stationary: tripSegment.stationaryAfterArrival, zoneType: tripSegment.arrivalZone.zoneType);
            print("Delegate - arrivalSuspected")
        }
        
        func arrived(_ tripSegment: PIOTripSegment) {
            self.insertEventViaDelegate(.arrived, location: tripSegment.arrivalLocation, transportationMode: tripSegment.transportationMode, stationary: tripSegment.stationaryAfterArrival, zoneType: tripSegment.arrivalZone.zoneType);
            print("Delegate - arrived")
        }
        func beingStationary(afterArrival tripSegment: PIOTripSegment!) {
            self.insertEventViaDelegate(.stationary, location: tripSegment.arrivalLocation, transportationMode: tripSegment.transportationMode, stationary: tripSegment.stationaryAfterArrival, zoneType: tripSegment.arrivalZone.zoneType);
            print("Delegate - searchingInPerimeter")
            
        }
        func searching(inPerimeter searchingLocation: CLLocation!) {
            self.insertEventViaDelegate(.searching, location: searchingLocation, transportationMode: TransportationMode.undetermined, stationary: false, zoneType: .other);
            print("Delegate - searchingInPerimeter")
    }
        func didUpdate(_ location: CLLocation!) {
             print("Delegate - didUpdateLocation")
}
